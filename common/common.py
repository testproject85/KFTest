import responses
import datetime
import requests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from dateutil import parser

def send_request_with_retry(request_type: int, body: dict, retries: int = 3):
    raise NotImplemented

def create_request_session_with_retries(retries: int = 3, backoff_factor: int = 0, status_forcelist: list = [ 500, 502, 503, 504 ]):
    session = requests.Session()
    retries = Retry(total=retries, backoff_factor=backoff_factor, status_forcelist=status_forcelist)
    session.mount('https://', HTTPAdapter(max_retries=retries))

    return session

def raise_breaking_request_exception(url, response, custom_failure_message):
    print(custom_failure_message)
    print(f'Request URL: {url}')
    print(f'HTTP Response: {response.status_code}')
    raise requests.ConnectionError(f"Expected status code 200, but got {response.status_code} with body {response.json()}")

def check_string_is_valid_datetime(datetime_string: str) -> str:
    try:
        res = bool(parser.parse(datetime_string))
    except ValueError:
        print("Invalid datetime string passed.")
        raise

if __name__ == '__main__':
    main()