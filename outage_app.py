import outages.outages as outages
import sites.sites as sites
import common.common as common
import datetime

def main():
    filter_datetime = '2022-01-01T00:00:00.000Z'
    filter_site = "norwich-pear-tree"

    all_outages = outages.get_all_outages()

    if not all_outages:
        print("No outages found, exiting.")
    else:
        site_info = sites.get_site_information(filter_site)

        date_filtered_outages = outages.filter_outage_information_by_date(all_outages, filter_datetime)
        device_enhanced_outages = outages.join_outages_to_site_devices(date_filtered_outages, site_info["devices"])

        if device_enhanced_outages:
            outages.send_enhanced_outage_information(device_enhanced_outages, filter_site)
        else:
            print("Empty device enhanced outages, skipping posting data.")

if __name__ == '__main__':
    main()