import config
import common.common as common

def get_site_information(site_id: int):
    api_key = config.API_KEY
    base_url = config.API_BASE_ROUTE
    full_request_path = f'{base_url}/site-info/{site_id}'
    headers = {"x-api-key": api_key}

    print(f'Attempting to retrieve site information for site {site_id}...')

    session = common.create_request_session_with_retries(retries=3, backoff_factor=1, status_forcelist=[500])
    response = session.get(full_request_path, headers=headers)
    if response.status_code != 200:
        custom_failure_message = f'Failed to get site information for site {site_id}'
        common.raise_breaking_request_exception(full_request_path, response, custom_failure_message)

    print(f'succesfully retrieved site information for site {site_id}.')

    return response.json()

if __name__ == '__main__':
    main()