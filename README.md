# About the app

This app has logic split into three main sections:

- common - Common functions to be used across multiple use cases
- outages - outage related functions
- sites - site related functions

Requirements are set via the pipenv configeration

# Configeration of the app

The following assumes you have python and pip installed, if not, install a 3.x version of python from https://www.python.org/.

To configure the appliction run the following commands:

```bash
pip install --user pipx
```

```bash
 pipx install pipenv
```

Place a file called config.py at the route of the repo, with the content (replace your api key into the file):

```python
API_KEY="<API KEY>"
API_BASE_ROUTE="https://api.krakenflex.systems/interview-tests-mock-api/v1"
```

Run the app with:

# Run the app

```bash
pipenv run python outage_app.py
```
# Run Unit Tests

Run test suite with:

```bash
pipenv run python -m pytest tests\ 
```

# Improvements

- Specify planned exit when no outages exist in system
- Directly load the swagger yaml into python and pull config from there
- Object Oriented for larger application/ease of additional logic
- Unit test API calls with mocking
- Better logging/centralised logging
- Better edge case handling (e.g. null values in the api responses)
- Add try/catch error logic
- split out common functions into more sensible seperations
- split out config file
- add some integration/feature tests
- More complicated timezone handling of time strings
- Schema checking on post payload
- Add function header comments to all functions
