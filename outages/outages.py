import datetime
import config
import common.common as common
import json
import requests
import pandas as pd

def get_all_outages() -> dict:
    api_key = config.API_KEY
    base_url = config.API_BASE_ROUTE
    full_request_path = f'{base_url}/outages'
    headers = {"x-api-key": api_key}

    print('Attempting to retrieve all outages...')

    session = common.create_request_session_with_retries(retries=3, backoff_factor=1, status_forcelist=[500])
    response = session.get(full_request_path, headers=headers)

    if response.status_code != 200:
        custom_failure_message = f'Failed to get all outages'
        common.raise_breaking_request_exception(full_request_path, response, custom_failure_message)

    print('succesfully retrieved all outages.')

    return response.json()

def send_enhanced_outage_information(enhanced_outages: list, site_id: str):
    api_key = config.API_KEY
    base_url = config.API_BASE_ROUTE
    full_request_path = f'{base_url}/site-outages/{site_id}'
    headers = {"x-api-key": api_key}

    print(f'Attempting to send enhanced outage information for site {site_id}...')

    session = common.create_request_session_with_retries(retries=3, backoff_factor=1, status_forcelist=[500])
    response = session.post(full_request_path, json=enhanced_outages, headers=headers)

    if response.status_code != 200:
        custom_failure_message = f'Failed to post enhanced outage information for site {site_id}'
        common.raise_breaking_request_exception(full_request_path, response, custom_failure_message)

    print(f'succesfully sent enhanced outage information for site {site_id}.')

def join_outages_to_site_devices(outages: list, devices: list) -> list:
    required_column_order = ['id',"name","begin","end"]
    
    if not outages:
        print("Empty outages list, returning empty list")
        return []
    elif not devices:
        print("Empty outages list, returning empty list")
        return []

    MergeColumn = 'id'
    df_outages = pd.DataFrame(outages)
    df_devices = pd.DataFrame(devices)

    merged_device_outages = pd.merge(df_outages, df_devices, on=MergeColumn)
    merged_device_outages_column_ordered = merged_device_outages[required_column_order]

    return merged_device_outages_column_ordered.to_dict('records')

def filter_outage_information_by_date(outages: list, filter_datetime: str) -> list:

    common.check_string_is_valid_datetime(filter_datetime)

    if len(outages) == 0:
        print("Empty outages list passed in, returning empty list")
        return []
    else:
        df_outages = pd.DataFrame(outages)
        df_filtered_outages = df_outages[(df_outages['begin'] >= filter_datetime)]

    return df_filtered_outages.to_dict('records')

if __name__ == '__main__':
    main()