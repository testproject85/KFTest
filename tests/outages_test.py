
import pytest
import responses
import outages.outages as outages
import json

def fake_outage_info():
    """Function that returns static outage data"""

    with open("tests/resources/outages.json") as outage_data:
        return json.load(outage_data)

def fake_device_info():
    """Function that returns static device data"""

    with open("tests/resources/devices.json") as device_data:
        return json.load(device_data)

def test_filter_outage_information_by_date():
    """Function tests that outage filter by date works as expected when passing valid values """

    filter_datetime = '2022-01-01T00:00:00.000Z'

    outage_data = fake_outage_info()
    filtered_outages = outages.filter_outage_information_by_date(outage_data, filter_datetime)

    assert len(filtered_outages) == 1
    assert filtered_outages[0]["begin"] ==  "2022-05-23T12:21:27.377Z"

def test_filter_outage_information_by_date_empty_outages():
    """Function tests that outage filter by date works as expected when empty outage information """

    filter_datetime = '2022-01-01T00:00:00.000Z'

    outage_data = []
    filtered_outages = outages.filter_outage_information_by_date(outage_data, filter_datetime)

    assert len(filtered_outages) == 0

def test_filter_outage_information_by_date_empty_after_filter():
    """Function tests that outage filter returns an empty list when the filter reduces the dataset to 0"""

    filter_datetime = '2024-01-01T00:00:00.000Z'

    outage = fake_outage_info()
    filtered_outages = outages.filter_outage_information_by_date(outage, filter_datetime)

    assert len(filtered_outages) == 0

def test_join_outages_to_site_devices():
    """Function tests the outages join to site devices logic works as expected when passed valid data"""
    
    outages_data = fake_outage_info()
    devices_data = fake_device_info()

    enhanced_outages = outages.join_outages_to_site_devices(outages_data, devices_data)

    assert len(enhanced_outages) == 1
    assert enhanced_outages[0]['id'] == '002b28fc-283c-47ec-9af2-ea287336dc1b'

def test_join_outages_to_site_devices_empty_outages():
    """Function tests the outages join to site devices logic works as expected when empty outages array"""
    outages_data = []
    devices_data = fake_device_info()

    enhanced_outages = outages.join_outages_to_site_devices(outages_data, devices_data)

    assert len(enhanced_outages) == 0

def test_join_outages_to_site_devices_empty_devices():
    """Function tests the outages join to site devices logic works as expected when empty devices array"""
    outages_data = fake_outage_info()
    devices_data = []

    enhanced_outages = outages.join_outages_to_site_devices(outages_data, devices_data)

    assert len(enhanced_outages) == 0