# Design for Kracken Backend Test

A brief approach/design is listed below:

- I plan to create this as a script with functions rather than to abstract into OOP classes
- I plan to do TDD and create the unit tests around each custom function first
- I plan to create three functions with unit tests around them:
  - a filter_outage_information_by_date function
  - an enhance_outage_information_with_site function
  - a send_request_with_retry function
- I plan to create three API wrapper functions without unit tests:
  - a get_all_outages function
  - a get_site_information function
  - a send_enhanced_outage_information function

I'll use pipenv for requirement/package lock.

To avoid spending too much time on config, I'll set environment variables (keys, base routes... etc) in a config file.